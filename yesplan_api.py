import requests
import json
from datetime import date, datetime, timedelta

API_KEY="undefined"

class Request():
    def __init__(self, *path):
        self.path = path

    @property
    def url(self):
        print(self.path)
        return f"http://hetpaleis.yesplan.be/api/{'/'.join(self.path)}?api_key={API_KEY}"

    @property
    def result(self):
        r = requests.get(self.url)
        if r.status_code != 200:
            raise Exception(f"Failed to fetch result for {self.url}")
        return json.loads(r.text)

    @property
    def data(self):
        r = self.result
        data = r['data']
        while r['pagination']:
            r = requests.get(r['next'])
            data += r['data']
        return data


class Location():
    def __init__(self, url, id, name, **kwargs):
        self.url = url
        self.id = id
        self.name = name

    @classmethod
    def all(c):
        data = Request("locations").data
        return [Location(**d) for d in data]

class Event():
    def __init__(self, url, id, name, starttime, endtime, locations, **kwargs):
        self.url = url
        self.id = id
        self.name = name
        self.start = datetime.fromisoformat(starttime)
        self.end = datetime.fromisoformat(endtime)
        self.duration = self.end - self.start
        if len(locations) != 1:
            raise Exception("An event can only be in 1 location")
        self.location = Location(**locations[0])

    def __repr__(self):
        return f"<yesplan.Event[{self.location.name}/{self.name}]@{self.start.isoformat()}[{self.id}]>"

    @classmethod
    def on(c, f_date):
        return [Event(**l) for l in Request("events", f"date:{f_date.strftime('%d-%m-%Y')}").data]

    @classmethod
    def today(c):
        return c.on(date.today())
