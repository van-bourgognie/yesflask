from os import getenv
from flask import Flask, render_template, request
from pprint import pprint
from pytz import timezone
from datetime import time, datetime, timedelta

import yesplan_api as yp
yp.API_KEY=getenv('YESPLAN_APIKEY')

app = Flask(__name__)
tzlocal = timezone('Europe/Brussels')

used_locations = [x.upper() for x in [
        "grote zaal",
        "kleine zaal",
        "hetgeluk",
        "10 repetitiezaal",
        "7 repetitiezaal"
        ]]

known_locations = [l.name.upper() for l in yp.Location.all()]
for l in used_locations:
    if l not in known_locations:
        raise Exception(f"unknown location {l}, not in {known_locations}")

@app.route("/")
def flat_table():
    try:
        now = datetime.fromisoformat(request.args.get('now'))
        if now.tzinfo == None:
            now = tzlocal.localize(now)
    except Exception as e:
        now = datetime.now(tzlocal)

    events = filter(lambda e:
        e.location.name.upper() in used_locations and
        e.end >= now,
        yp.Event.on(now.date()))

    events = sorted(events, key=lambda x: x.start)

    return render_template('index.html', events=events)
